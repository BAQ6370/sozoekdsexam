
## code to prepare `adult_income dataset goes here

library(devtools)
library(roxygen2)

adult_income <- read.csv("Z://wiso/SozOek_EmpWifo/Gemeinsam/L_Projekte/Data_Science_Kurs_WiSe_2023_24/Package/sozoekdsexam/data-raw/adult_income.csv")

save(adult_income, file = "Z://wiso/SozOek_EmpWifo/Gemeinsam/L_Projekte/Data_Science_Kurs_WiSe_2023_24/Package/sozoekdsexam/data/adult_income.rda")


usethis::use_data(adult_income, overwrite = TRUE)