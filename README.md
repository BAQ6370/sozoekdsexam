# sozoekds

Package for the exam of the course "Data Science for Socioeconomists" at the department of Socioeconomics at University of Hamburg, Germany. Includes the following data sets:

-   *adult_income* with census data of american households regarding their income and sociodemographic characteristics.

-   *airbnbbig* with airbnb data from London, the prices and characteristics of rentals (full sample, all variables)

-   *airbnbsmall* is a smaller version of the airbnbbig dataset and includes only a part of the full sample

-   *calhouse* includes information about the housing market in california and variables like prices, composition and distance to major cities

-   *census_2015* with aggregated census data of american households regarding their income and sociodemographic characteristics.

-   *examscores2* is an updated version of *examscores*. It includes information about educational data and models a fictional school and its students

-   *income_data* includes information about people's age and their income - it models a fictional population

## Installation

Either:\
#install.packages("remotes")\
#library(remotes)\
remotes::install_gitlab("BAQ6370/sozoekdsexam", host="gitlab.rrz.uni-hamburg.de", force = TRUE)

or:

#install.packages("devtools")\
#library(devtools)\
devtools::install_git("[https://gitlab.rrz.uni-hamburg.de/baq6370/sozoekdsexam.git](https://gitlab.rrz.uni-hamburg.de/bay1977/sozoeko1.git)", force = TRUE)

## Support

If you find a bug please contact: [lisamarie.wegner\@uni-hamburg.de](lisamarie.wegner@uni-hamburg.de)

## Roadmap

Package should be used in teaching Datascience for Socioeconomists at WiSo-Fakultaet, area Socioeconomics at Hamburg University in Winter 2023.

## Authors and acknowledgment

Lisa Wegner

## License

CC BY-NC-ND

## Project status

Active
